edit your hosts file and add : 
	192.168.0.169	localhost.com
	192.168.0.169   help
	192.168.0.169   local.aetvdigital.com
    192.168.0.169	local.mylifetime.com local.movies.mylifetime.com
	etc...

Clone this vagrant box repo to your <devbox directory>

START VAGRANT BOX
> cd <devbox directory>/_OperaBox
> vagrant up --provider=virtualbox

The box setup can be seen at:
http://help/
phpMyadmin is accessible at:
http://help/sql

adding sites:
place sites in: localhost/
set up virtualhost in : -OperaBox/data/VirtualHosts