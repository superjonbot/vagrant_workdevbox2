node "doco6-d7.devopera" {

file { "/var/www/html/localhost":
    ensure => "directory",
    owner  => "web",
    group  => "www-data",
    mode   => 777,
}

file { '/var/www/html/localhost/index.php':
    ensure => "file",
    owner  => "web",
    group  => "www-data",
    mode   => "777",
    content => "<? phpinfo(); ?>",
    require => File["/var/www/html/localhost"],
}

exec { "restartapache":
    command => "/sbin/service httpd restart",
    path    => "/usr/local/bin/:/bin/",
    user => root,
    require => File["/var/www/html/localhost/index.php"],
}


} 