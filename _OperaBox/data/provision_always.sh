#!/usr/bin/env bash
echo "Shell Provisioning start"

#Turn off SELINUX
sudo cp -f /data/Configs/selinux/config /etc/selinux
sudo setenforce 0

#remove logfiles (help sql restart?)
sudo rm -rf /var/lib/mysql/ib_logfile*

#get VirtualHosts
sudo cp /data/VirtualHosts/vhost-* /etc/httpd/conf.d

#get HOSTSFILE
sudo cp -f /data/Configs/hosts /etc
sudo cp -f /data/Configs/.bash_profile /home/web


sudo cp -Rfu /data/puppetmodules/* /usr/share/puppet/modules


sudo netstat -tap | grep LISTEN
echo $(sudo netstat -tap | grep memcached)
echo $(php -i | grep "Loaded Configuration File")
echo "puppet node: $(facter | grep fqdn)"
echo "Shell Provisioning end"

# sudo memcached -p 19111 -d
# sudo memcached -p 19112 -d
# sudo memcached -p 19113 -d
# sudo memcached -p 19114 -d
# sudo memcached -p 19115 -d
# sudo memcached -p 19116 -d
# sudo memcached -p 19117 -d
# sudo memcached -p 19118 -d
# sudo memcached -p 19119 -d

#aetv
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19111
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19112
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19113
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19114
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19115
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19116
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19117
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19118
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19119

#mylifetime
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19120
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19121
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19122
memcached -d -u nobody -m 32 -l 127.0.0.1 -p 19123
#
#drush user-create superjonbot --mail="delsoul@gmail.com" --password="pa55w0rd"
#drush user-add-role administrator superjonbot
