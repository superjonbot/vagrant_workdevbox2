#!/usr/bin/env bash
echo "Shell Provisioning start"
sudo cp /data/Configs/php.ini /usr/local/zend/etc

#make helpful symlinks
sudo ln -s /var/www/html/localhost /localhost


#kill built in drupal site
sudo rm -rf /var/www/html/dodrupal-base-7
sudo rm /etc/httpd/conf.d/vhost-00-drupal-7.conf
sudo rm /home/web/drupal-7

cd /tmp
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

cd /localhost



#install php-mcrypt
sudo yum install php-mcrypt* -y

echo "Install Memcached"
sudo yum install memcached -y
sudo service memcached start
echo "stats settings" | nc localhost 11211
sudo yum install autoconf -y
sudo pecl channel-update pecl.php.net
printf "\n" | sudo pecl install memcache
sudo sed -i -e '$a\' /usr/local/zend/etc/php.ini
echo "extension=memcache.so" | sudo tee -a /usr/local/zend/etc/php.ini

#install puppet
yes | sudo yum -y install puppet
#install sshpass
sudo yum install sshpass -y
#Install Nano
sudo yum install nano -y

#sass/compass/node/grunt
sudo gem update --system -y
sudo gem install sass -y
sudo gem install compass -y
sudo yum install inotify-tools -y
sudo yum install nodejs -y
sudo yum install nodejs npm -y
sudo npm install -g inherits
sudo npm install -g grunt
sudo npm install -g -y grunt-cli

sudo npm install -g grunt --save-dev
sudo npm install -g grunt-banner --save-dev
sudo npm install -g grunt-contrib-concat --save-dev
sudo npm install -g grunt-contrib-copy --save-dev
sudo npm install -g grunt-contrib-jshint --save-dev
sudo npm install -g grunt-contrib-requirejs --save-dev
sudo npm install -g grunt-contrib-uglify --save-dev
sudo npm install -g grunt-contrib-watch --save-dev
sudo npm install -g grunt-jsbeautifier --save-dev
sudo npm install -g grunt-notify --save-dev
sudo npm install -g grunt-shell --save-dev

